# Getting started with AMQ Broker Operator on OpenShift Container Platform

This guide shows you how to send and receive messages using the amqp protocol to
an artemis broker deployed in openshift

## Prerequisites

* You must have access to an OpenShift instance and be logged in.

* You must have the AMQ Operator installed in the either the working project or across all namespaces.

## Deploying the services on OpenShift

1. Use the `oc new-project` command to create a new namespace for the
   example services.

        oc new-project amq-demo

1. If you haven't already, use `git` to clone the example code to your
   local environment.

        git clone https://gitlab.consulting.redhat.com/cmcswain/ocp-amq-demo

1. Change directory to the example project.  The subsequent commands
   assume it is your current directory.

        cd ocp-amq-demo/ocp

1. Create an AMQ Artemis Broker using the operator.

        oc create -f amqp-broker-instance.yaml

1. Use the `oc apply` command to load the sender and receiver templates.

        oc apply -f amq-demo-sender.yaml
        oc apply -f amq-demo-receiver.yaml

1. Deploy the message sender service.

        oc new-app --template=amq-demo-sender

1. Deploy the message receiver service.

        oc new-app --template=amq-demo-receiver

1. Use your browser to check the status of your services.  You should
   see three services ("applications") in the overview, each with one
   pod.

## Exercising the application

The application exposes two HTTP endpoints, one for sending messages
and one for receiving them.

    http://<sender-host>/api/send
    http://<receiver-host>/api/receive

The `<sender-host>` and `<receiver-host>` values vary with each
deployment.  Use the web interface to find the precise values.  They
are listed on the right side of each service ("application") listed in
the overview.

To send a message, use the `curl` command.  The value you supply for
the `string` field is used as the message payload.

    curl --location --request POST 'http://<sender-host>/api/send' --header 'Content-Type: text/plain' --header 'Cookie: 1e3ee5a8a558bb0126bd636a0bcc52ae=471eab100f5f28591bf65adbf5bc47ca' -d 'Hello'

If things go as planned, it will return `OK`.  If things go awry, add
the `-v` flag to see more about what's happening.

To then receive the string back, use the `curl` command again against
the receiver.  If no message is available, it will print `null`.

    curl -X POST http://<receiver-host>/api/receive

Upon success, you should see the message you sent echoed back in the
response.  Here's some sample output from a few operations:
