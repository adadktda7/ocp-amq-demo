apiVersion: v1
kind: Template
metadata:
  labels:
    app: amq-demo-sender
  name: amq-demo-sender
parameters:
  - name: SOURCE_REPOSITORY_URL
    description: The source URL for the application
    displayName: Source URL
    value: ssh://git@gitlab.consulting.redhat.com:2222/cmcswain/ocp-amq-demo.git
    required: true
  - name: SOURCE_REPOSITORY_REF
    description: The branch name for the application
    displayName: Source Branch
    value: master
    required: true
  - name: SOURCE_REPOSITORY_DIR
    description: The location within the source repo of the application
    displayName: Source Directory
    value: sender
    required: true
  - name: ARTIFACT_COPY_ARGS
    description: Syntax to be used to copy uberjar files to the target directory
    displayName: Copy Args
    value: '*-jar-with-dependencies.jar'
    required: true
  - name: GITHUB_WEBHOOK_SECRET
    description: A secret string used to configure the GitHub webhook.
    displayName: GitHub Webhook Secret
    required: true
    from: '[a-zA-Z0-9]{40}'
    generate: expression
  - name: MAVEN_MIRROR_URL
    description: Maven Nexus Repository to be used during build phase
    displayName:
    required: false
objects:
- apiVersion: v1
  kind: ImageStream
  metadata:
    labels:
      app: amq-demo-sender
    name: amq-demo-sender
  spec:
    lookupPolicy:
      local: true
- apiVersion: v1
  kind: ImageStream
  metadata:
    labels:
      app: amq-demo-sender
    name: runtime-amq-demo-sender
  spec:
    tags:
    - name: latest
      from:
        kind: DockerImage
        name: registry.access.redhat.com/redhat-openjdk-18/openjdk18-openshift
- apiVersion: v1
  kind: BuildConfig
  metadata:
    labels:
      app: amq-demo-sender
    name: amq-demo-sender
  spec:
    output:
      to:
        kind: ImageStreamTag
        name: amq-demo-sender:latest
    postCommit: {}
    resources: {}
    source:
      git:
        uri: ${SOURCE_REPOSITORY_URL}
        ref: ${SOURCE_REPOSITORY_REF}
      sourceSecret:
        name: gitlab-key
      type: Git
    strategy:
      sourceStrategy:
        from:
          kind: ImageStreamTag
          name: runtime-amq-demo-sender:latest
        incremental: true
        env:
        - name: MAVEN_ARGS_APPEND
          value: "--projects ${SOURCE_REPOSITORY_DIR}"
        - name: ARTIFACT_DIR
          value: "${SOURCE_REPOSITORY_DIR}/target"
        - name: MAVEN_MIRROR_URL
          value: "${MAVEN_MIRROR_URL}"
        - name: ARTIFACT_COPY_ARGS
          value: "${ARTIFACT_COPY_ARGS}"
      type: Source
    triggers:
    - github:
        secret: ${GITHUB_WEBHOOK_SECRET}
      type: GitHub
    - type: ConfigChange
    - imageChange: {}
      type: ImageChange
  status:
    lastVersion: 0
- apiVersion: apps/v1
  kind: Deployment
  metadata:
    labels:
      app: amq-demo-sender
    name: amq-demo-sender
  spec:
    replicas: 1
    revisionHistoryLimit: 2
    selector:
      matchLabels:
        app: amq-demo-sender
    strategy:
      rollingUpdate:
        maxSurge: 25%
        maxUnavailable: 25%
      type: RollingUpdate
    template:
      metadata:
        labels:
          app: amq-demo-sender
      spec:
        containers:
        - env:
          - name: KUBERNETES_NAMESPACE
            valueFrom:
              fieldRef:
                fieldPath: metadata.namespace
          envFrom:
          - configMapRef:
              name: messaging-service
          image: amq-demo-sender:latest
          imagePullPolicy: Always
          livenessProbe:
            httpGet:
              path: /api/health
              port: 8080
              scheme: HTTP
            initialDelaySeconds: 180
          name: amq-demo-sender
          ports:
          - containerPort: 8080
            name: http
            protocol: TCP
          readinessProbe:
            httpGet:
              path: /api/health
              port: 8080
              scheme: HTTP
            initialDelaySeconds: 10
          securityContext:
            privileged: false
- apiVersion: v1
  kind: Service
  metadata:
    labels:
      app: amq-demo-sender
    name: amq-demo-sender
  spec:
    ports:
    - name: http
      port: 8080
    selector:
      app: amq-demo-sender
- apiVersion: v1
  kind: Route
  metadata:
    labels:
      app: amq-demo-sender
      deployment: amq-demo-sender
    name: sender
  spec:
    port:
      targetPort: 8080
    to:
      kind: Service
      name: amq-demo-sender

